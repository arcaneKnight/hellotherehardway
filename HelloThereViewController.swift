//
//  HelloThereViewController.swift
//  helloThereHard
//
//  Created by 黃善勇 on 31-10-15.
//  Copyright © 2015 Shanyung IT. All rights reserved.
//

import UIKit

class HelloThereViewController: UIViewController {

    var helloLabel: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor.lightGrayColor()
        
        helloLabel = UILabel(frame: CGRect(x:20,y:20,width: 200,height:150))
        helloLabel?.text = "Hello, world"
       
        helloLabel?.backgroundColor = UIColor.yellowColor()
        
        self.view.addSubview(helloLabel!)

        let hiButton: UIButton = UIButton(frame: CGRect(x:20,y:250,width:50,height:30))
        hiButton.setTitle("Hi",forState: UIControlState.Normal)
        hiButton.addTarget(self, action:"sayhi", forControlEvents: UIControlEvents.TouchUpInside)
        hiButton.addTarget(self, action:"sayhi", forControlEvents: UIControlEvents.TouchUpOutside)
        

        self.view.addSubview(hiButton)
        
        let byeButton: UIButton  = UIButton(frame: CGRect(x:250,y:250,width:50,height:30))
        
        byeButton.setTitle("Bye",forState: UIControlState.Normal)
        byeButton.addTarget(self, action:"saybye", forControlEvents: UIControlEvents.TouchUpInside)

        
        self.view.addSubview(byeButton)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func sayhi() {
        print("sayhi")
        helloLabel?.text = "Hello there again!"
        
    }
    
    func saybye() {
        print("saybye")
        helloLabel?.text = "See you soon"
    }

}
